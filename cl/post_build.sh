﻿#!/bin/sh -u

# Properties for Cloud Build
# WORKSPACE=""
# BUILD_TARGET="iOS"
# AWS_ACCESS_KEY=""
# AWS_SECRET_KEY=""
# S3_BUCKET="apps-general"
# S3_FOLDER="ab-world-ebooks-test"

# Inputs
MIME="application/octet-stream"
S3_REGION="eu-west-2"                                                       
S3_ACL="public-read"

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# set the path based on the first argument
path="${WORKSPACE}/.build/last/${BUILD_TARGET}/extra_data/addrs/"

echo "\n"
echo "Uploading Files at $path to S3"
echo "\n"

# loop through the folder path and upload each file
for f in "$path"/*
do
    # get the file
    file="${f##*/}"

    # build the file path
    filePath="$path/$file"

    echo "Uploading File: $file"

    # upload the file to s3
    sh "${__dir}"/post_build_upload_to_s3_1.sh "$AWS_ACCESS_KEY" "$AWS_SECRET_KEY" "$S3_BUCKET" "$S3_REGION" "$filePath" "$S3_FOLDER/$BUILD_TARGET/$file" "$S3_ACL" "$MIME"

    echo "\n"
done

echo "3S Upload Complete to: $S3_BUCKET.s3.amazonaws.com/$S3_FOLDER/"