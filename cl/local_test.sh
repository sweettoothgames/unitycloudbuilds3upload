﻿#!/bin/sh -u

# Inputs
WORKSPACE=""
BUILD_TARGET="iOS"
MIME="application/octet-stream"
AWS_ACCESS_KEY="***************"
AWS_SECRET_KEY="***************"
S3_REGION="eu-west-2"                                                       
S3_BUCKET="apps-general"                                                    
S3_FOLDER="ab-world-ebooks-test"
S3_ACL="public-read"

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# set the path based on the first argument
#path="${WORKSPACE}/.build/last/${BUILD_TARGET}/extra_data/addrs/"
path="/Users/chris/Work/BLZ001_alphablocks_app/Development/Unity - Alphablocks/SVOD-Alphablock/ServerData/iOS"

echo "\n"
echo "Uploading Files at $path to S3"
echo "\n"

for f in "$path"/*
do
    file="${f##*/}"
    filePath="$path/$file"
    echo "Uploading File: $file"
    sh "${__dir}"/post_build_upload_to_s3_1.sh "$AWS_ACCESS_KEY" "$AWS_SECRET_KEY" "$S3_BUCKET" "$S3_REGION" "$filePath" "$S3_FOLDER/$BUILD_TARGET/$file" "$S3_ACL" "$MIME"
    echo "\n"
done

echo "3S Upload Complete to: $S3_BUCKET.s3.amazonaws.com/$S3_FOLDER/"